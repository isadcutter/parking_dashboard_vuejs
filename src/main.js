import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Main from './components/Main.vue'
// import Login from './components/Login.vue'
const router = new VueRouter({
  mode: "history",
  base: '/parking',
  routes: [
    // { path: '/', name: 'Login', component: Login, props: true },
    { path: '/', name: 'Main', component: Main, props: true },
    { path: '/main', name: 'Main', component: Main, props: true },
  ]
})


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
